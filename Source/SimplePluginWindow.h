#ifndef SIMPLE_PLUGINWINDOW_H_INCLUDED
#define SIMPLE_PLUGINWINDOW_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class SimplePluginWindow  : public DocumentWindow
{
    SimplePluginWindow (Component* const uiComp,
                        AudioProcessorGraph::Node* owner_,
                        const bool isGeneric_);

public:
    
    ~SimplePluginWindow();
    
    void moved();
    void closeButtonPressed();
    
    static SimplePluginWindow* getWindowFor (AudioProcessorGraph::Node* node,
                                             bool useGenericView);
private:
    AudioProcessorGraph::Node* owner;
    bool isGeneric;

    
};


#endif  // SIMPLE_PLUGINWINDOW_H_INCLUDED

